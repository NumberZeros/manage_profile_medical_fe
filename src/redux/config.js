import { ApolloClient, HttpLink, ApolloLink, InMemoryCache, concat } from '@apollo/client';

const httpLink = new HttpLink({ uri: 'http://localhost:8080/graphql' });

const authMiddleware = new ApolloLink((operation, forward) => {
  // add the authorization to the headers
  operation.setContext(({ headers = {} }) => ({
    headers: {
      ...headers,
      authorization: "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6ImRlMzc1MjU3LTIwMjAtNGE2Zi1hODY5LTljMzQ1OThkZjcyNSIsImlhdCI6MTYyNzcxMDAyMSwiZXhwIjoxNjI3NzExODIxfQ.ee8z5yDiVsSawc7nNXtdBuLuYvX_HSF6ZE-bVKw9sXk" || null,
    }
  }));
  return forward(operation);
})

export const client = new ApolloClient({
  link: concat(authMiddleware, httpLink),
  cache: new InMemoryCache(),
});

import { createStore, applyMiddleware, compose } from "redux";
import { createLogger } from "redux-logger";
import { routerMiddleware } from "connected-react-router";
import { createBrowserHistory } from "history";

import reducers from "./reducer.js";

export const history = createBrowserHistory();

const middleware = routerMiddleware(history);
const loggerMiddleware = createLogger({
  predicate: () => process.env.NODE_ENV === "development",
});
const middlewares = [middleware, loggerMiddleware];

export default function configureStore(initialState) {
  const store = createStore(
    reducers(history),
    initialState,
    compose(applyMiddleware(...middlewares))
  );

  return {
    store,
    history,
  };
}

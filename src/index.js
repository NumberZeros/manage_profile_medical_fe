/*!

=========================================================
* Material Dashboard React - v1.10.0
=========================================================

* Product Page: https://www.creative-tim.com/product/material-dashboard-react
* Copyright 2021 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/material-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";
import { Provider } from "react-redux";

// core components
import Admin from "layouts/Admin.js";
import SignIn from "./views/Authenticate/Authenticate.js"
//apollo
import "assets/css/material-dashboard-react.css?v=1.10.0";
import { ApolloProvider } from "@apollo/client";
import { client } from "./redux/config";
import { ConnectedRouter } from "connected-react-router";
import configureStore from "./redux/store";

function App() {
  const { store, history } = configureStore([]);
  return (
    <ApolloProvider client={client}>
      <Provider store={store}>
        <ConnectedRouter history={history}>
          <BrowserRouter>
            <Switch>
              <Route path="/admin" component={Admin} />
              <Route path="/login" component={SignIn} />
              <Redirect from="/" to="/admin/dashboard" />
            </Switch>
          </BrowserRouter>
        </ConnectedRouter>
      </Provider>
    </ApolloProvider>
  );
}

ReactDOM.render(<App />, document.getElementById("root"));

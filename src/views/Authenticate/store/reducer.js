/**
 * @file reducer
 */

// Using to control stage

import freeze from "deep-freeze";
import { handleActions } from "redux-actions";
import * as actions from "./actions";

export const name = "Login";

const initialState = freeze({
  data: null,
  token: null,
  userInfo: null,
  isAuthenticated: false,
  error: null,
  isWrong: null,
  isSuccess: null,
  isLoading: null,
});

export default handleActions(
  {
    [actions.handleLogin]: (state) => {
      return freeze({
        ...state,
        isLoading: true,
      });
    },
    [actions.handleLoginSuccess]: (state, actions) => {
      console.log(actions.payload.data.token)
      return freeze({
        ...state,
        isSuccess: true,
        isLoading: false,
      });
    },
    [actions.handleLoginFail]: (state, action) => {
      return freeze({
        ...state,
        error: action.payload.error,
        isSuccess: false,
        isLoading: false,
      });
    },
  },
  initialState
);
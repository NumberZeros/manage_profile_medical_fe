import { createAction } from "redux-actions";

export const handleLogin = createAction("AUTHENTICATE/LOGIN");
export const handleLoginSuccess = createAction("AUTHENTICATE/LOGIN_SUCCESS");
export const handleLoginFail = createAction("AUTHENTICATE/LOGIN_FAIL");

export const handleGetInfo = createAction("AUTHENTICATE/GET_INFO");
export const handleGetInfoSuccess = createAction("AUTHENTICATE/GET_INFO_SUCCESS");
export const handleGetInfoFail = createAction("AUTHENTICATE/GET_INFO_FAIL");

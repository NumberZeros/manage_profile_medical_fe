import { gql } from "@apollo/client";

export const login = gql`
  mutation Login($email: String!, $password: String!) {
    login_user(input: {email: $email, password: $password}) {
			access_token,
			refresh_token,
			id
		}
  }
`;
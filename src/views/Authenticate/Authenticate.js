import React, { useState } from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import { useForm } from "react-hook-form";
import * as actions  from "./store/actions";
import { useSelector, useDispatch } from "react-redux";
import { useMutation } from "@apollo/client";
import * as apis from "./store/api"
import { name } from "./store/reducer";

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="https://material-ui.com/">
        Your Website
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

const LOGIN = apis.login;

export default function SignIn() {
  const dispatch = useDispatch();
  const { isLoading, isSuccess } = useSelector((state) => state[name]);  console.log(isLoading)
  const classes = useStyles();
  const [Login, { data, error, loading }] = useMutation(LOGIN, { errorPolicy: 'all' });
  const [err, setErr] = useState(false);
  const { register, handleSubmit } = useForm();
  const onSubmit = async (data) =>{
    try {
      Login({ variables: { email: data.email, password: data.password }})
      dispatch(actions.handleLogin(result))
      if(!isLoading && isSuccess) dispatch(actions.handleLoginSuccess(result))
      else dispatch(actions.handleLoginFail(result))
    } catch (e) {
      setErr(e);
      e.preventDefault();
    }
  }
  const result = {
    data: data,
    error: error
  }
  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign in
        </Typography>
        <form onSubmit={handleSubmit(onSubmit)} className={classes.form}>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="email"
            label="Email Address"
            autoComplete="email"
            autoFocus
            {...register("email")}
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            {...register("password")}
            label="Password"
            type="password"
            id="password"
          />
          {!loading && err && <pre><span>{result.error?.message}</span></pre>}
          <FormControlLabel
            control={<Checkbox value="remember" color="primary" />}
            label="Remember me"
          />
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            Sign In
          </Button>
          <Grid container>
            <Grid item xs>
              <Link href="#" variant="body2">
                Forgot password?
              </Link>
            </Grid>
            <Grid item>
              <Link href="#" variant="body2">
                {"Don't have an account? Sign Up"}
              </Link>
            </Grid>
          </Grid>
        </form>
      </div>
      <Box mt={8}>
        <Copyright />
      </Box>
    </Container>
  );
}